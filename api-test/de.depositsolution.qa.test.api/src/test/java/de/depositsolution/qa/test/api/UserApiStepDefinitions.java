package de.depositsolution.qa.test.api;

import static com.jayway.restassured.RestAssured.given;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import de.depositsolution.qa.test.api.commons.Constants;
import de.depositsolution.qa.test.api.models.UserModel;
import io.cucumber.datatable.DataTable;

/**
 * Contains step definitions for user api test.
 * 
 * @author ramya
 *
 */
public class UserApiStepDefinitions {

	private static final Logger LOG = LoggerFactory.getLogger(UserApiStepDefinitions.class);

	private String url;
	private Response response;

	@Given("APIs are running at {string}")
	public void apis_are_running_at(String url) {
		LOG.debug("Initializing target url");
		this.url = url;
	}

	@When("a user performs a {string} request to {string}")
	public void a_user_performs_a_request_to(String requestType, String path) {
		LOG.debug("Starting url request");
		a_user_performs_a_request_to_with_request_data(requestType, path, null);
	}

	@When("a user performs a {string} request to {string} with request data")
	public void a_user_performs_a_request_to_with_request_data(String requestType, String path, DataTable dataTable) {

		// preparing request
		RequestSpecification requestSpec = given();

		// setting request data
		if (dataTable != null) {
			String requestBody = new JSONObject(dataTable.asMap(String.class, String.class)).toString();
			requestSpec = requestSpec.body(requestBody);
		}

		performRequest(requestType, path, requestSpec);
	}

	/**
	 * Performs appropriate http request for a given request type.
	 * 
	 * @param requestSpec rest assured request specification
	 * @return response
	 */
	private Response performRequest(String requestType, String apiPath, RequestSpecification requestSpec) {
		String requestUrl = url + apiPath;

		switch (requestType.toUpperCase()) {
		case Constants.HTTP_METHOD_POST:
			this.response = requestSpec.when().post(requestUrl);
			break;

		case Constants.HTTP_METHOD_DELETE:
			this.response = requestSpec.when().delete(requestUrl);
			break;

		default:
			this.response = requestSpec.when().get(requestUrl);
		}

		return this.response;
	}

	@Then("the response code should be {int}")
	public void the_response_code_should_be(int expectedStatusCode) {
		LOG.debug("Checking response status");
		Assertions.assertTrue(response.getStatusCode() == expectedStatusCode, "Checking response status");
	}

	@Then("error message must be {string}")
	public void error_message_must_be(String errorMsg) {
		LOG.debug("Checking for error message {}", errorMsg);
		if (400 == response.getStatusCode()) {
			Assertions.assertEquals(errorMsg, response.jsonPath().getString("error"), "Checking error message");
		}
	}

	@Then("the response array must contain {int} users")
	public void the_response_array_must_contain_users(int expectedNoOfUsers) {
		LOG.debug("Checking all added users {} are available or not", expectedNoOfUsers);
		List<Object> userList = this.response.jsonPath().getList(".");
		Assertions.assertEquals(expectedNoOfUsers, userList.size(), "Checking no. of users");
	}

	@Then("the response should contain right user info")
	public void the_response_should_contain_right_user_info(DataTable dataTable) {
		LOG.debug("Checking added user info are stored correct");
		List<UserModel> userList = this.response.jsonPath().getList(".", UserModel.class);

		Map<String, String> expectedData = dataTable.asMap(String.class, String.class);
		Assertions.assertEquals(new UserModel(expectedData), userList.get(0), "Checking user info");
	}
}
