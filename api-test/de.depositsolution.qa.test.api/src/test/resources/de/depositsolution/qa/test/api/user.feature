@user
Feature: User APIs
  This feature is used to check the behaviour of user APIs

  @save
  Scenario Outline: Checking save user API - success flow
    Given APIs are running at "http://85.93.17.135:9000"
    When a user performs a "DELETE" request to "/user/all"
    Then the response code should be 200
    When a user performs a "GET" request to "/user/all/json"
    Then the response code should be 200
    And the response array must contain 0 users
    When a user performs a "POST" request to "/user/save/json" with request data
      | name                 | <name>          |
      | email                | <email>         |
      | password             | <pwd>           |
      | confirmationPassword | <confirmed_pwd> |
    Then the response code should be 200
    When a user performs a "GET" request to "/user/all/json"
    Then the response code should be 200
    And the response array must contain 1 users
    And the response should contain right user info
      | name                 | <name>          |
      | email                | <email>         |
      | password             | <pwd>           |
      | confirmationPassword | <confirmed_pwd> |
    When a user performs a "DELETE" request to "/user/all"
    Then the response code should be 200
    When a user performs a "GET" request to "/user/all/json"
    Then the response code should be 200
    And the response array must contain 0 users

    Examples: 
      | name  | email           | pwd       | confirmed_pwd | response_code | error_msg |
      | user1 | user1@email.com | user1@123 | user1@123     |           200 |           |

  @save
  Scenario Outline: Checking save user API - negative case
    Given APIs are running at "http://85.93.17.135:9000"
    When a user performs a "POST" request to "/user/save/json" with request data
      | name                 | <name>          |
      | email                | <email>         |
      | password             | <pwd>           |
      | confirmationPassword | <confirmed_pwd> |
    Then the response code should be <response_code>
    And error message must be "<error_msg>"

    Examples: 
      | name   | email            | pwd        | confirmed_pwd | response_code | error_msg                   |
      | user2  | user2@email.com  | user2@123  |               |           400 | Mandatory parameter missing |
      | user3  | user3@email.com  |            |               |           400 | Mandatory parameter missing |
      | user4  |                  | user4@123  | user4@123     |           400 | Mandatory parameter missing |
      |        | user5@email.com  | user5@123  | user5@123     |           400 | Mandatory parameter missing |
      |        |                  | user6@123  | user6@123     |           400 | Mandatory parameter missing |
      |        |                  |            |               |           400 | Mandatory parameter missing |
      |        |                  |            | user7@123     |           400 | Mandatory parameter missing |
      | user22 | user22email.com  | user22@123 | user22@123    |           400 | Incorrect email address     |
      | user23 | user23@email.com | user23@123 | user23@456    |           400 | Password not matching       |
