package de.depositsolution.qa.test.api.commons;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains global constant variables used across the project.
 * 
 * @author ramya
 *
 */
public final class Constants {

	public static final String SAVE_USER_API = "/user/save/json";
	public static final String GET_ALL_USERS_API = "/user/all/json";
	public static final String DELETE_ALL_USERS_API = "/user/all";

	public static final String HTTP_METHOD_GET = "GET";
	public static final String HTTP_METHOD_POST = "POST";
	public static final String HTTP_METHOD_DELETE = "DELETE";

	public static final Map<String, String> apiHttpMethodMap;

	static {
		apiHttpMethodMap = new HashMap<>();
		apiHttpMethodMap.put(SAVE_USER_API, HTTP_METHOD_POST);
		apiHttpMethodMap.put(GET_ALL_USERS_API, HTTP_METHOD_GET);
		apiHttpMethodMap.put(DELETE_ALL_USERS_API, HTTP_METHOD_DELETE);
	}
}
