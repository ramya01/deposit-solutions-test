package de.depositsolution.qa.test.api.models;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Model for user detail.
 * 
 * @author ramya
 *
 */
public class UserModel {

	private static final Logger LOG = LoggerFactory.getLogger(UserModel.class);

	private String name;
	private String email;
	private String password;
	private String confirmationPassword;

	public UserModel() {
		LOG.debug("Initializing model");
	}

	public UserModel(Map<String, String> map) {
		LOG.debug("Initializing model with given map {}", map);
		this.name = map.get("name");
		this.email = map.get("email");
		this.password = map.get("password");
		this.confirmationPassword = map.get("confirmationPassword");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmationPassword() {
		return confirmationPassword;
	}

	public void setConfirmationPassword(String confirmationPassword) {
		this.confirmationPassword = confirmationPassword;
	}

	@Override
	public boolean equals(Object obj) {
		UserModel obj2 = (UserModel) obj;
		if (StringUtils.equals(this.name, obj2.name) && StringUtils.equals(this.email, obj2.email)) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.name.hashCode();
	}

	@SuppressWarnings("unchecked")
	public static UserModel cast(Object obj) {
		LOG.debug("Perform object casting to user model");
		return new UserModel((HashMap<String, String>) obj);

	}

}
