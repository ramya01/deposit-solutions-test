$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("classpath:de/depositsolution/qa/test/api/user.feature");
formatter.feature({
  "name": "User APIs",
  "description": "  This feature is used to check the behaviour of user APIs",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@user"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Checking save user API - success flow",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@save"
    }
  ]
});
formatter.step({
  "name": "APIs are running at \"http://85.93.17.135:9000\"",
  "keyword": "Given "
});
formatter.step({
  "name": "a user performs a \"DELETE\" request to \"/user/all\"",
  "keyword": "When "
});
formatter.step({
  "name": "the response code should be 200",
  "keyword": "Then "
});
formatter.step({
  "name": "a user performs a \"GET\" request to \"/user/all/json\"",
  "keyword": "When "
});
formatter.step({
  "name": "the response code should be 200",
  "keyword": "Then "
});
formatter.step({
  "name": "the response array must contain 0 users",
  "keyword": "And "
});
formatter.step({
  "name": "a user performs a \"POST\" request to \"/user/save/json\" with request data",
  "keyword": "When ",
  "rows": [
    {
      "cells": [
        "name",
        "\u003cname\u003e"
      ]
    },
    {
      "cells": [
        "email",
        "\u003cemail\u003e"
      ]
    },
    {
      "cells": [
        "password",
        "\u003cpwd\u003e"
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        "\u003cconfirmed_pwd\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "the response code should be 200",
  "keyword": "Then "
});
formatter.step({
  "name": "a user performs a \"GET\" request to \"/user/all/json\"",
  "keyword": "When "
});
formatter.step({
  "name": "the response code should be 200",
  "keyword": "Then "
});
formatter.step({
  "name": "the response array must contain 1 users",
  "keyword": "And "
});
formatter.step({
  "name": "the response should contain right user info",
  "keyword": "And ",
  "rows": [
    {
      "cells": [
        "name",
        "\u003cname\u003e"
      ]
    },
    {
      "cells": [
        "email",
        "\u003cemail\u003e"
      ]
    },
    {
      "cells": [
        "password",
        "\u003cpwd\u003e"
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        "\u003cconfirmed_pwd\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "a user performs a \"DELETE\" request to \"/user/all\"",
  "keyword": "When "
});
formatter.step({
  "name": "the response code should be 200",
  "keyword": "Then "
});
formatter.step({
  "name": "a user performs a \"GET\" request to \"/user/all/json\"",
  "keyword": "When "
});
formatter.step({
  "name": "the response code should be 200",
  "keyword": "Then "
});
formatter.step({
  "name": "the response array must contain 0 users",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "name",
        "email",
        "pwd",
        "confirmed_pwd",
        "response_code",
        "error_msg"
      ]
    },
    {
      "cells": [
        "user1",
        "user1@email.com",
        "user1@123",
        "user1@123",
        "200",
        ""
      ]
    }
  ]
});
formatter.scenario({
  "name": "Checking save user API - success flow",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@user"
    },
    {
      "name": "@save"
    }
  ]
});
formatter.step({
  "name": "APIs are running at \"http://85.93.17.135:9000\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UserApiStepDefinitions.apis_are_running_at(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"DELETE\" request to \"/user/all\"",
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 200",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"GET\" request to \"/user/all/json\"",
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 200",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response array must contain 0 users",
  "keyword": "And "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_array_must_contain_users(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"POST\" request to \"/user/save/json\" with request data",
  "rows": [
    {
      "cells": [
        "name",
        "user1"
      ]
    },
    {
      "cells": [
        "email",
        "user1@email.com"
      ]
    },
    {
      "cells": [
        "password",
        "user1@123"
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        "user1@123"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to_with_request_data(String,String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 200",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"GET\" request to \"/user/all/json\"",
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 200",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response array must contain 1 users",
  "keyword": "And "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_array_must_contain_users(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response should contain right user info",
  "rows": [
    {
      "cells": [
        "name",
        "user1"
      ]
    },
    {
      "cells": [
        "email",
        "user1@email.com"
      ]
    },
    {
      "cells": [
        "password",
        "user1@123"
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        "user1@123"
      ]
    }
  ],
  "keyword": "And "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_should_contain_right_user_info(DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"DELETE\" request to \"/user/all\"",
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 200",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"GET\" request to \"/user/all/json\"",
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 200",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response array must contain 0 users",
  "keyword": "And "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_array_must_contain_users(int)"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Checking save user API - negative case",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@save"
    }
  ]
});
formatter.step({
  "name": "APIs are running at \"http://85.93.17.135:9000\"",
  "keyword": "Given "
});
formatter.step({
  "name": "a user performs a \"POST\" request to \"/user/save/json\" with request data",
  "keyword": "When ",
  "rows": [
    {
      "cells": [
        "name",
        "\u003cname\u003e"
      ]
    },
    {
      "cells": [
        "email",
        "\u003cemail\u003e"
      ]
    },
    {
      "cells": [
        "password",
        "\u003cpwd\u003e"
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        "\u003cconfirmed_pwd\u003e"
      ]
    }
  ]
});
formatter.step({
  "name": "the response code should be \u003cresponse_code\u003e",
  "keyword": "Then "
});
formatter.step({
  "name": "error message must be \"\u003cerror_msg\u003e\"",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "name",
        "email",
        "pwd",
        "confirmed_pwd",
        "response_code",
        "error_msg"
      ]
    },
    {
      "cells": [
        "user2",
        "user2@email.com",
        "user2@123",
        "",
        "400",
        "Mandatory parameter missing"
      ]
    },
    {
      "cells": [
        "user3",
        "user3@email.com",
        "",
        "",
        "400",
        "Mandatory parameter missing"
      ]
    },
    {
      "cells": [
        "user4",
        "",
        "user4@123",
        "user4@123",
        "400",
        "Mandatory parameter missing"
      ]
    },
    {
      "cells": [
        "",
        "user5@email.com",
        "user5@123",
        "user5@123",
        "400",
        "Mandatory parameter missing"
      ]
    },
    {
      "cells": [
        "",
        "",
        "user6@123",
        "user6@123",
        "400",
        "Mandatory parameter missing"
      ]
    },
    {
      "cells": [
        "",
        "",
        "",
        "",
        "400",
        "Mandatory parameter missing"
      ]
    },
    {
      "cells": [
        "",
        "",
        "",
        "user7@123",
        "400",
        "Mandatory parameter missing"
      ]
    },
    {
      "cells": [
        "user22",
        "user22email.com",
        "user22@123",
        "user22@123",
        "400",
        "Incorrect email address"
      ]
    },
    {
      "cells": [
        "user23",
        "user23@email.com",
        "user23@123",
        "user23@456",
        "400",
        "Password not matching"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Checking save user API - negative case",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@user"
    },
    {
      "name": "@save"
    }
  ]
});
formatter.step({
  "name": "APIs are running at \"http://85.93.17.135:9000\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UserApiStepDefinitions.apis_are_running_at(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"POST\" request to \"/user/save/json\" with request data",
  "rows": [
    {
      "cells": [
        "name",
        "user2"
      ]
    },
    {
      "cells": [
        "email",
        "user2@email.com"
      ]
    },
    {
      "cells": [
        "password",
        "user2@123"
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        ""
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to_with_request_data(String,String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 400",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Checking response status\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat de.depositsolution.qa.test.api.UserApiStepDefinitions.the_response_code_should_be(UserApiStepDefinitions.java:91)\r\n\tat ✽.the response code should be 400(classpath:de/depositsolution/qa/test/api/user.feature:45)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "error message must be \"Mandatory parameter missing\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserApiStepDefinitions.error_message_must_be(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Checking save user API - negative case",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@user"
    },
    {
      "name": "@save"
    }
  ]
});
formatter.step({
  "name": "APIs are running at \"http://85.93.17.135:9000\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UserApiStepDefinitions.apis_are_running_at(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"POST\" request to \"/user/save/json\" with request data",
  "rows": [
    {
      "cells": [
        "name",
        "user3"
      ]
    },
    {
      "cells": [
        "email",
        "user3@email.com"
      ]
    },
    {
      "cells": [
        "password",
        ""
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        ""
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to_with_request_data(String,String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 400",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Checking response status\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat de.depositsolution.qa.test.api.UserApiStepDefinitions.the_response_code_should_be(UserApiStepDefinitions.java:91)\r\n\tat ✽.the response code should be 400(classpath:de/depositsolution/qa/test/api/user.feature:45)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "error message must be \"Mandatory parameter missing\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserApiStepDefinitions.error_message_must_be(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Checking save user API - negative case",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@user"
    },
    {
      "name": "@save"
    }
  ]
});
formatter.step({
  "name": "APIs are running at \"http://85.93.17.135:9000\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UserApiStepDefinitions.apis_are_running_at(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"POST\" request to \"/user/save/json\" with request data",
  "rows": [
    {
      "cells": [
        "name",
        "user4"
      ]
    },
    {
      "cells": [
        "email",
        ""
      ]
    },
    {
      "cells": [
        "password",
        "user4@123"
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        "user4@123"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to_with_request_data(String,String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 400",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Checking response status\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat de.depositsolution.qa.test.api.UserApiStepDefinitions.the_response_code_should_be(UserApiStepDefinitions.java:91)\r\n\tat ✽.the response code should be 400(classpath:de/depositsolution/qa/test/api/user.feature:45)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "error message must be \"Mandatory parameter missing\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserApiStepDefinitions.error_message_must_be(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Checking save user API - negative case",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@user"
    },
    {
      "name": "@save"
    }
  ]
});
formatter.step({
  "name": "APIs are running at \"http://85.93.17.135:9000\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UserApiStepDefinitions.apis_are_running_at(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"POST\" request to \"/user/save/json\" with request data",
  "rows": [
    {
      "cells": [
        "name",
        ""
      ]
    },
    {
      "cells": [
        "email",
        "user5@email.com"
      ]
    },
    {
      "cells": [
        "password",
        "user5@123"
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        "user5@123"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to_with_request_data(String,String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 400",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Checking response status\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat de.depositsolution.qa.test.api.UserApiStepDefinitions.the_response_code_should_be(UserApiStepDefinitions.java:91)\r\n\tat ✽.the response code should be 400(classpath:de/depositsolution/qa/test/api/user.feature:45)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "error message must be \"Mandatory parameter missing\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserApiStepDefinitions.error_message_must_be(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Checking save user API - negative case",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@user"
    },
    {
      "name": "@save"
    }
  ]
});
formatter.step({
  "name": "APIs are running at \"http://85.93.17.135:9000\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UserApiStepDefinitions.apis_are_running_at(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"POST\" request to \"/user/save/json\" with request data",
  "rows": [
    {
      "cells": [
        "name",
        ""
      ]
    },
    {
      "cells": [
        "email",
        ""
      ]
    },
    {
      "cells": [
        "password",
        "user6@123"
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        "user6@123"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to_with_request_data(String,String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 400",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Checking response status\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat de.depositsolution.qa.test.api.UserApiStepDefinitions.the_response_code_should_be(UserApiStepDefinitions.java:91)\r\n\tat ✽.the response code should be 400(classpath:de/depositsolution/qa/test/api/user.feature:45)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "error message must be \"Mandatory parameter missing\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserApiStepDefinitions.error_message_must_be(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Checking save user API - negative case",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@user"
    },
    {
      "name": "@save"
    }
  ]
});
formatter.step({
  "name": "APIs are running at \"http://85.93.17.135:9000\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UserApiStepDefinitions.apis_are_running_at(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"POST\" request to \"/user/save/json\" with request data",
  "rows": [
    {
      "cells": [
        "name",
        ""
      ]
    },
    {
      "cells": [
        "email",
        ""
      ]
    },
    {
      "cells": [
        "password",
        ""
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        ""
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to_with_request_data(String,String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 400",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Checking response status\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat de.depositsolution.qa.test.api.UserApiStepDefinitions.the_response_code_should_be(UserApiStepDefinitions.java:91)\r\n\tat ✽.the response code should be 400(classpath:de/depositsolution/qa/test/api/user.feature:45)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "error message must be \"Mandatory parameter missing\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserApiStepDefinitions.error_message_must_be(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Checking save user API - negative case",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@user"
    },
    {
      "name": "@save"
    }
  ]
});
formatter.step({
  "name": "APIs are running at \"http://85.93.17.135:9000\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UserApiStepDefinitions.apis_are_running_at(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"POST\" request to \"/user/save/json\" with request data",
  "rows": [
    {
      "cells": [
        "name",
        ""
      ]
    },
    {
      "cells": [
        "email",
        ""
      ]
    },
    {
      "cells": [
        "password",
        ""
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        "user7@123"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to_with_request_data(String,String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 400",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Checking response status\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat de.depositsolution.qa.test.api.UserApiStepDefinitions.the_response_code_should_be(UserApiStepDefinitions.java:91)\r\n\tat ✽.the response code should be 400(classpath:de/depositsolution/qa/test/api/user.feature:45)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "error message must be \"Mandatory parameter missing\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserApiStepDefinitions.error_message_must_be(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Checking save user API - negative case",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@user"
    },
    {
      "name": "@save"
    }
  ]
});
formatter.step({
  "name": "APIs are running at \"http://85.93.17.135:9000\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UserApiStepDefinitions.apis_are_running_at(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"POST\" request to \"/user/save/json\" with request data",
  "rows": [
    {
      "cells": [
        "name",
        "user22"
      ]
    },
    {
      "cells": [
        "email",
        "user22email.com"
      ]
    },
    {
      "cells": [
        "password",
        "user22@123"
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        "user22@123"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to_with_request_data(String,String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 400",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Checking response status\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat de.depositsolution.qa.test.api.UserApiStepDefinitions.the_response_code_should_be(UserApiStepDefinitions.java:91)\r\n\tat ✽.the response code should be 400(classpath:de/depositsolution/qa/test/api/user.feature:45)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "error message must be \"Incorrect email address\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserApiStepDefinitions.error_message_must_be(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.scenario({
  "name": "Checking save user API - negative case",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@user"
    },
    {
      "name": "@save"
    }
  ]
});
formatter.step({
  "name": "APIs are running at \"http://85.93.17.135:9000\"",
  "keyword": "Given "
});
formatter.match({
  "location": "UserApiStepDefinitions.apis_are_running_at(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a user performs a \"POST\" request to \"/user/save/json\" with request data",
  "rows": [
    {
      "cells": [
        "name",
        "user23"
      ]
    },
    {
      "cells": [
        "email",
        "user23@email.com"
      ]
    },
    {
      "cells": [
        "password",
        "user23@123"
      ]
    },
    {
      "cells": [
        "confirmationPassword",
        "user23@456"
      ]
    }
  ],
  "keyword": "When "
});
formatter.match({
  "location": "UserApiStepDefinitions.a_user_performs_a_request_to_with_request_data(String,String,DataTable)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the response code should be 400",
  "keyword": "Then "
});
formatter.match({
  "location": "UserApiStepDefinitions.the_response_code_should_be(int)"
});
formatter.result({
  "error_message": "java.lang.AssertionError: Checking response status\r\n\tat org.junit.Assert.fail(Assert.java:88)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat de.depositsolution.qa.test.api.UserApiStepDefinitions.the_response_code_should_be(UserApiStepDefinitions.java:91)\r\n\tat ✽.the response code should be 400(classpath:de/depositsolution/qa/test/api/user.feature:45)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "error message must be \"Password not matching\"",
  "keyword": "And "
});
formatter.match({
  "location": "UserApiStepDefinitions.error_message_must_be(String)"
});
formatter.result({
  "status": "skipped"
});
});