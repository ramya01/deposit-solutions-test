package de.depositsolution.qa.test.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.depositsolution.qa.test.ui.commons.Commons;
import de.depositsolution.qa.test.ui.elements.Text;
import de.depositsolution.qa.test.ui.locators.NewUserPageLocators;

/**
 * Contains common page behaviors.
 * 
 * @author ramya
 *
 */
public class BasePage {

	private static Logger LOG = LoggerFactory.getLogger(BasePage.class);
	protected WebDriver driver;

	@FindBy(css = NewUserPageLocators.PAGE_HEADER)
	private WebElement pageHeader;

	public BasePage() {
		LOG.debug("Initialing New User Page");
		this.driver = Commons.getDriver(false);
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	/**
	 * Returns page header.
	 * 
	 * @return page header text
	 */
	public final String getPageHeader() {
		LOG.debug("Getting page header");
		return new Text(pageHeader).getText();
	}

}
