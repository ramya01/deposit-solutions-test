package de.depositsolution.qa.test.ui.elements;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to create text component for a given element.
 * 
 * @author ramya
 *
 */
public class Text {

	private static Logger LOG = LoggerFactory.getLogger(Text.class);
	private WebElement element;

	public Text(WebElement element) {
		LOG.debug("Initiating Text component");
		this.element = element;
	}

	/**
	 * Returns body text.
	 * 
	 * @return text
	 */
	public String getText() {
		return element.getText();
	}
}
