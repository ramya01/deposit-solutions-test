package de.depositsolution.qa.test.ui.elements;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to create button component for a element.
 * 
 * @author ramya
 *
 */
public class Button {

	private static Logger LOG = LoggerFactory.getLogger(Button.class);
	private WebElement element;

	public Button(WebElement element) {
		LOG.debug("Initiating button component");
		this.element = element;
	}

	/**
	 * Clicks on button.
	 */
	public void click() {
		LOG.debug("Clicking button");
		element.click();
	}
}
