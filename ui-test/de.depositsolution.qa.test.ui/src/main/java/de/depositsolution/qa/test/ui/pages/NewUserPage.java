package de.depositsolution.qa.test.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.depositsolution.qa.test.ui.commons.Commons;
import de.depositsolution.qa.test.ui.elements.Button;
import de.depositsolution.qa.test.ui.elements.TextField;
import de.depositsolution.qa.test.ui.locators.NewUserPageLocators;

/**
 * Page class for New user page.
 * 
 * @author ramya
 *
 */
public class NewUserPage extends BasePage {

	private static final Logger LOG = LoggerFactory.getLogger(NewUserPage.class);

	@FindBy(css = NewUserPageLocators.TEXTFIELD_NAME)
	private WebElement textFieldName;

	@FindBy(css = NewUserPageLocators.TEXTFIELD_EMAIL)
	private WebElement textFieldEmail;

	@FindBy(css = NewUserPageLocators.TEXTFIELD_PASSWORD)
	private WebElement textFieldPassword;

	@FindBy(css = NewUserPageLocators.TEXTFIELD_CONFIRMATION_PASSWORD)
	private WebElement textFieldConfirmationPassword;

	@FindBy(css = NewUserPageLocators.BUTTON_ALL_USER)
	private WebElement buttonAllUser;

	@FindBy(css = NewUserPageLocators.BUTTON_SUMBIT)
	private WebElement buttonSubmit;

	@FindBy(xpath = NewUserPageLocators.ERROR_USER_NAME)
	public WebElement errorUserName;

	@FindBy(xpath = NewUserPageLocators.ERROR_USER_EMAIL)
	public WebElement errorEmail;

	@FindBy(xpath = NewUserPageLocators.ERROR_USER_PASSWORD)
	public WebElement errorPassword;

	@FindBy(xpath = NewUserPageLocators.ERROR_USER_CONFIRMATION_PASSWORD)
	public WebElement errorConfirmPassword;

	public NewUserPage() {
		LOG.debug("Initialing New User Page");
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	/**
	 * Sets user name.
	 * 
	 * @param name name to be set
	 * @return true on success, otherwise false
	 */
	public final boolean setName(String name) {
		LOG.debug("Setting user name {}", name);
		return new TextField(textFieldName).setText(name);
	}

	/**
	 * Returns user name which has set.
	 * 
	 * @return user name
	 */
	public final String getName() {
		LOG.debug("Getting user name");
		return new TextField(textFieldName).getText();
	}

	/**
	 * Sets email address.
	 * 
	 * @param email email address to be set
	 * @return true on success, otherwise false
	 */
	public final boolean setEmail(String email) {
		LOG.debug("Setting mobile or email {}", email);
		return new TextField(textFieldEmail).setText(email);
	}

	/**
	 * Returns email address which has set.
	 * 
	 * @return email address
	 */
	public final String getEmail() {
		LOG.debug("Getting email address");
		return new TextField(textFieldEmail).getText();
	}

	/**
	 * Sets password.
	 * 
	 * @param pwd password to be set
	 * @return true on success, otherwise false
	 */
	public final boolean setPassword(String pwd) {
		LOG.debug("Setting password {}", pwd);
		return new TextField(textFieldPassword).setText(pwd);
	}

	/**
	 * Returns password which has set.
	 * 
	 * @return password
	 */
	public final String getPassword() {
		LOG.debug("Getting password");
		return new TextField(textFieldPassword).getText();
	}

	/**
	 * Sets confirmation password.
	 * 
	 * @param pwd password to be set
	 * @return true on success, otherwise false
	 */
	public final boolean setConfirmationPassword(String pwd) {
		LOG.debug("Setting confirmation password {}", pwd);
		return new TextField(textFieldConfirmationPassword).setText(pwd);
	}

	/**
	 * Returns confirmation password which has set.
	 * 
	 * @return password
	 */
	public final String getConfirmationPassword() {
		LOG.debug("Getting confirmation password");
		return new TextField(textFieldConfirmationPassword).getText();
	}

	/**
	 * Clicks All user button.
	 */
	public final void clickAllUserButton() {
		LOG.debug("Clicking All user button");
		new Button(buttonAllUser).click();
	}

	/**
	 * Clicks submit button.
	 */
	public final void clickSubmit() {
		LOG.debug("Clicking submit button");
		new Button(buttonSubmit).click();
	}

	/**
	 * Returns error message if user name validation fails.
	 * 
	 * @return error message
	 */
	public final String getUserNameErrorMessage() {
		LOG.debug("Getting user name validation error message");
		return driver.findElement(By.xpath(NewUserPageLocators.ERROR_USER_NAME)).getText().trim();
	}

	/**
	 * Returns error message if user email validation fails.
	 * 
	 * @return error message
	 */
	public final String getEmailErrorMessage() {
		LOG.debug("Getting email validation error message");
		return driver.findElement(By.xpath(NewUserPageLocators.ERROR_USER_EMAIL)).getText().trim();
	}

	/**
	 * Returns error message if password validation fails.
	 * 
	 * @return error message
	 */
	public final String getPasswordErrorMessage() {
		LOG.debug("Getting password validation error message");
		return driver.findElement(By.xpath(NewUserPageLocators.ERROR_USER_PASSWORD)).getText().trim();
	}

	/**
	 * Returns error message if confirmation password validation fails.
	 * 
	 * @return error message
	 */
	public final String getConfirmationPasswordErrorMessage() {
		LOG.debug("Getting confirmation password validation error message");
		return driver.findElement(By.xpath(NewUserPageLocators.ERROR_USER_CONFIRMATION_PASSWORD)).getText().trim();
	}

	/**
	 * Fills and submit new user form.
	 * 
	 * @param name       user name
	 * @param email      user email
	 * @param pwd        password
	 * @param confirmPwd confirmation password
	 */
	public void addUser(String name, String email, String pwd, String confirmPwd) {
		LOG.debug("Filling new user form with name: {}, email: {}, password: {}, confirmationPassword: {}", name, email,
				pwd, confirmPwd);
		setName(name);
		setEmail(email);
		setPassword(pwd);
		setConfirmationPassword(confirmPwd);
		Commons.takeSnapShot();
		clickSubmit();
	}
}
