package de.depositsolution.qa.test.ui.locators;

/**
 * Contains locators of All user page.
 * 
 * @author ramya
 *
 */
public final class AllUserPageLocators {

	public static final String BUTTON_NEW_USER = ".btn-default";
	public static final String TABLE_ALL_USER_HEADER = "//table[@id='users']/thead/tr/th";
	public static final String TABLE_ALL_USER_RECORD = "//table[@id='users']/tbody/tr";
}
