package de.depositsolution.qa.test.ui.locators;

/**
 * Contains locators of New user page.
 * 
 * @author ramya
 *
 */
public final class NewUserPageLocators {

	public static final String PAGE_HEADER = ".page-header";
	public static final String TEXTFIELD_NAME = "#registrationForm #name";
	public static final String TEXTFIELD_EMAIL = "#registrationForm #email";
	public static final String TEXTFIELD_PASSWORD = "#registrationForm #password";
	public static final String TEXTFIELD_CONFIRMATION_PASSWORD = "#registrationForm #confirmationPassword";
	public static final String BUTTON_ALL_USER = "#registrationForm .form-actions a[class^='btn']";
	public static final String BUTTON_SUMBIT = "#registrationForm .form-actions button[type='submit']";
	public static final String ERROR_USER_NAME = "//form[@id='registrationForm']//p[@id='user.name.error']";
	public static final String ERROR_USER_EMAIL = "//form[@id='registrationForm']//p[@id='user.email.error']";
	public static final String ERROR_USER_PASSWORD = "//form[@id='registrationForm']//p[@id='user.password.error']";
	public static final String ERROR_USER_CONFIRMATION_PASSWORD = "//form[@id='registrationForm']//p[@id='user.confirmationPassword.error']";
}
