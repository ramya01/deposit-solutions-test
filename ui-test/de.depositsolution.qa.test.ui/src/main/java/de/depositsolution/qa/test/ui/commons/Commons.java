package de.depositsolution.qa.test.ui.commons;

import static com.jayway.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.restassured.response.Response;

import de.depositsolution.qa.test.ui.models.UserModel;

/**
 * Contains common functionalities used across the project.
 * 
 * @author ramya
 *
 */
public final class Commons {

	private static Logger LOG = LoggerFactory.getLogger(Commons.class);
	private static final int POLLING_INT = 100;
	private static final int WAIT_TIMEOUT = 20;

	static {
		System.setProperty("webdriver.chrome.driver", Constants.CHROME_DRIVER_PATH);
	}

	private static WebDriver driver;

	/**
	 * Returns a web driver.
	 * 
	 * @return web driver object
	 */
	public static final WebDriver getDriver(boolean freshDriver) {
		LOG.debug("Getting webdriver");
		// supported only chrome. To be done for other browsers.
		if (freshDriver || null == driver) {
			LOG.debug("Getting fresh webdriver");
			driver = new ChromeDriver();
			// driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		return driver;
	}

	public static final int deleteUsers() {
		Response response = given().when().delete(Constants.DELETE_ALL_USER_API);
		return response.getStatusCode();
	}

	public static final List<UserModel> getUsers() {
		Response response = given().when().get(Constants.GET_ALL_USER_API);
		List<UserModel> userList = response.jsonPath().getList(".", UserModel.class);
		return userList;
	}

	public static final WebDriverWait getWait() {
		LOG.debug("Getting webdriver wait");
		return new WebDriverWait(driver, WAIT_TIMEOUT, POLLING_INT);
	}

	public static void takeSnapShot() {
		LOG.debug("Taking screenshot");
		try {
			TakesScreenshot screenShot = ((TakesScreenshot) driver);
			File srcFile = screenShot.getScreenshotAs(OutputType.FILE);
			File destFile = new File(
					"./target/surefire-reports/snapshots/snapshot" + System.currentTimeMillis() + ".png");
			FileUtils.copyFile(srcFile, destFile);

		} catch (IOException e) {
			LOG.error("Error taking screenshot", e);
		}
	}

	public static final void driverCleanup() {
		LOG.debug("Started cleanup");
		if (driver != null) {
			driver.close();
			driver.quit();
		}
	}
}
