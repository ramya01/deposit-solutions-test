package de.depositsolution.qa.test.ui.commons;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wait implementation for ensuring the state of web elements.
 * 
 * @author ramya
 *
 */
public final class Waiter {

	private static Logger LOG = LoggerFactory.getLogger(Waiter.class);

	/**
	 * * Waits until web element present.
	 * 
	 * @param locator xpath of webelement
	 * @return web element
	 */
	public static WebElement waitUntilElementPresent(String locator) {
		LOG.debug("Waiting for element's presence - " + locator);
		return Commons.getWait().until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
	}

	/**
	 * Waits until web element is getting selected.
	 * 
	 * @param element web element
	 * @return true on success, otherwise false
	 */
	public static boolean waitUntilElementSelected(WebElement element) {
		LOG.debug("Waiting for element's selection - " + element.toString());
		return Commons.getWait().until(ExpectedConditions.elementToBeSelected(element));
	}

	/**
	 * Waits until web element selection state is getting changed to a given state.
	 * 
	 * @param element web element
	 * @param state   web element state
	 * @return true on success, otherwise false
	 */
	public static boolean waitUntilElementSelectionStateToBe(WebElement element, boolean state) {
		LOG.debug("Waiting for element's state change - locator: " + element.toString() + " state: " + state);
		return Commons.getWait().until(ExpectedConditions.elementSelectionStateToBe(element, true));
	}

	/**
	 * Waits until web element text is getting changed to a given text.
	 * 
	 * @param element web element
	 * @param text    web element text
	 * @return true on success, otherwise false
	 */
	public static boolean waitUntilElementTextToBe(WebElement element, String text) {
		LOG.debug("Waiting for element's text change - locator: " + element.toString() + " text: " + text);
		return Commons.getWait().until(ExpectedConditions.textToBePresentInElementValue(element, text));
	}

	/**
	 * Waits until url.
	 * 
	 * @param url redirect url
	 * @return true on success, otherwise false
	 */
	public static boolean waitUntilUrlToBe(String url) {
		LOG.debug("Waiting for URL redirect - {}", url);
		Boolean success = Commons.getWait().until(ExpectedConditions.urlToBe(url));
		Commons.takeSnapShot();
		return success;
	}
}
