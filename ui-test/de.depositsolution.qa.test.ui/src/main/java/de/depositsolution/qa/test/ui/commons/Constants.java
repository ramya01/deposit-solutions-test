package de.depositsolution.qa.test.ui.commons;

/**
 * Contains global constant variables used across the project.
 * 
 * @author ramya
 *
 */
public final class Constants {

	public static final String NEW_USER_PAGE_URL = "http://85.93.17.135:9000/user/new";
	public static final String ALL_USER_PAGE_URL = "http://85.93.17.135:9000/users/all";
	public static final String DELETE_ALL_USER_API = "http://85.93.17.135:9000/user/all";
	public static final String GET_ALL_USER_API = "http://85.93.17.135:9000/user/all/json";
	public static final String CHROME_DRIVER_PATH = "./drivers/chromedriver.exe";

	public static final int RESPONSE_200 = 200;

	public static final String ERROR_MSG_MUST_BE_UNIQUE = "Must be unique";
	public static final String ERROR_MSG_INVALID_EMAIL_ADDRESS = "Invalid email address";
	public static final String ERROR_MSG_REQUIRED = "Required";
	public static final String ERROR_MSG_PWD_MIN_SIZE = "Minimum size is 6";
	public static final String ERROR_MSG_PWD_NOT_SAME = "passwords are not the same";

	public static final String FIELD_NAME = "name";
	public static final String FIELD_EMAIL = "email";
	public static final String FIELD_PASSWORD = "password";
	public static final String FIELD_CONFIRMATION_PASSWORD = "confirmationPassword";

	public static final String MSG_NO_USER = "no users";
	public static final String PAGE_HEADER_NEW_USER_PAGE = "new user";
	public static final String PAGE_HEADER_ALL_USER_PAGE = "all user";
}
