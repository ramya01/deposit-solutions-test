package de.depositsolution.qa.test.ui.elements;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.depositsolution.qa.test.ui.commons.Waiter;

/**
 * This class is used to create text field component for a given element.
 * 
 * @author ramya
 *
 */
public class TextField {

	private static Logger LOG = LoggerFactory.getLogger(TextField.class);
	private WebElement element;

	public TextField(WebElement element) {
		LOG.debug("Initiating textfield component");
		this.element = element;
	}

	/**
	 * Sets text into text field.
	 * 
	 * @param text text to be set
	 * @return true on success, otherwise false
	 */

	public boolean setText(String text) {
		LOG.debug("Typing text");
		element.sendKeys(text);
		return Waiter.waitUntilElementTextToBe(element, text);
	}

	/**
	 * Returns text field value.
	 * 
	 * @return value
	 */
	public String getText() {
		return element.getText();
	}
}
