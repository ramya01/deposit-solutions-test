package de.depositsolution.qa.test.ui.elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.depositsolution.qa.test.ui.commons.Constants;

/**
 * This class is used to create table component for a given elements.
 * 
 * @author ramya
 *
 */
public class Table {

	private static Logger LOG = LoggerFactory.getLogger(Table.class);
	private List<WebElement> headerElements;
	private List<WebElement> recordElements;

	public Table(List<WebElement> headerElements, List<WebElement> recordElements) {
		LOG.debug("Initiating table component");
		this.headerElements = headerElements;
		this.recordElements = recordElements;
	}

	/**
	 * Returns table headers.
	 * 
	 * @return headers
	 */
	public final List<String> getHeaders() {
		LOG.debug("Fetching headers from table");
		return headerElements.stream().map(x -> x.getText().trim()).collect(Collectors.toList());
	}

	/**
	 * Returns table data.
	 * 
	 * @return records
	 */
	public final List<Map<String, String>> getRecords() {
		LOG.debug("Fetching records from table");

		List<Map<String, String>> records = new ArrayList<>();
		List<String> headers = getHeaders();
		recordElements.forEach(x -> {
			List<WebElement> cells = x.findElements(By.xpath("./td"));
			Map<String, String> recordMap = IntStream.range(0, cells.size()).boxed()
					.collect(Collectors.toMap(i -> headers.get(i), i -> cells.get(i).getText()));
			records.add(recordMap);
		});

		LOG.debug("No. of records presents in table {}", records.size());
		LOG.debug("Records presents in table {}", records.toString());

		return records;
	}

	/**
	 * Returns true, if there is no users.
	 * 
	 * @return true, if no users. Otherwise, false
	 */
	public final boolean isNoUsers() {
		LOG.debug("Checking table has content or not");

		if (1 == recordElements.size()) {
			List<WebElement> tableCell = recordElements.get(0).findElements(By.xpath("./td"));
			if (1 == tableCell.size()
					&& StringUtils.equalsIgnoreCase(Constants.MSG_NO_USER, tableCell.get(0).getText())) {
				return true;
			}
		}
		return false;
	}
}
