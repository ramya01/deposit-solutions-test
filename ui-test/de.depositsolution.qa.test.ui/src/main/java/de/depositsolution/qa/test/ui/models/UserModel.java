package de.depositsolution.qa.test.ui.models;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * Model for user record.
 * 
 * @author ramya
 *
 */
public class UserModel {

	private String name;
	private String email;
	private String password;
	private String confirmationPassword;

	public UserModel(String name, String email, String password, String confirmationPassword) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.confirmationPassword = confirmationPassword;
	}

	public UserModel(Map<String, String> map) {
		this.name = map.get("name");
		this.email = map.get("email");
		this.password = map.get("password");
		this.confirmationPassword = map.get("confirmationPassword");
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getConfirmationPassword() {
		return confirmationPassword;
	}

	@Override
	public boolean equals(Object obj) {
		UserModel obj2 = (UserModel) obj;
		if (StringUtils.equals(this.name, obj2.name) && StringUtils.equals(this.email, obj2.email)) {
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.name.hashCode();
	}

	@SuppressWarnings("unchecked")
	public static UserModel cast(Object obj) {
		return new UserModel((HashMap<String, String>) obj);

	}

}
