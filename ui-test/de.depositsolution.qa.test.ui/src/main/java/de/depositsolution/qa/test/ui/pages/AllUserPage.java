package de.depositsolution.qa.test.ui.pages;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.depositsolution.qa.test.ui.elements.Button;
import de.depositsolution.qa.test.ui.elements.Table;
import de.depositsolution.qa.test.ui.locators.AllUserPageLocators;

/**
 * Page class for All user page.
 * 
 * @author ramya
 *
 */
public class AllUserPage extends BasePage {

	private static final Logger LOG = LoggerFactory.getLogger(AllUserPage.class);

	@FindBy(css = AllUserPageLocators.BUTTON_NEW_USER)
	private WebElement buttonNewUser;

	@FindBy(xpath = AllUserPageLocators.TABLE_ALL_USER_HEADER)
	private List<WebElement> tableHeaders;

	@FindBy(xpath = AllUserPageLocators.TABLE_ALL_USER_RECORD)
	private List<WebElement> tableRecords;

	private Table table;

	public AllUserPage() {
		LOG.debug("Initialing All User Page");
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
		table = new Table(tableHeaders, tableRecords);
	}

	/**
	 * Returns table headers.
	 * 
	 * @return table headers
	 */
	public final List<String> getTableHeaders() {
		LOG.debug("Getting table records from table component");
		return table.getHeaders();
	}

	/**
	 * Returns table records.
	 * 
	 * @return table records
	 */
	public final List<Map<String, String>> getTableRecords() {
		LOG.debug("Getting table records from table component");
		return table.getRecords();
	}

	/**
	 * Clicks new user button.
	 */
	public final void clickNewUserButton() {
		LOG.debug("Clicking submit button");
		new Button(buttonNewUser).click();
	}

	/**
	 * Checks user record availability.
	 * 
	 * @return true, if any user records is available. Otherwise, false.
	 */
	public final boolean isAnyUserRecords() {
		return !table.isNoUsers();
	}
}
