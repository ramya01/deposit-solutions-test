package de.depositsolution.qa.test.ui;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.depositsolution.qa.test.ui.commons.Constants;
import de.depositsolution.qa.test.ui.commons.Waiter;
import de.depositsolution.qa.test.ui.pages.AllUserPage;
import de.depositsolution.qa.test.ui.pages.NewUserPage;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;

/**
 * Test for All user page.
 * 
 * @author ramya
 *
 */
@Epic("All user page")
@Feature("All users table")
public class AllUserPageTest extends BasePageTest {
	private AllUserPage allUserPage;

	@BeforeEach
	public void openPage() {
		driver.navigate().to(Constants.ALL_USER_PAGE_URL);
		allUserPage = new AllUserPage();
		String pageHeader = allUserPage.getPageHeader();
		Assertions.assertTrue(StringUtils.equalsIgnoreCase("all user", pageHeader), "Checking New user page header");
	}

	@Test
	@Story("All user table data")
	@Description("Add some users and check added users are shown in all user table")
	public void testAllUserTable() {
		// Checking no users scenario
		Assertions.assertFalse(allUserPage.isAnyUserRecords(), "Checking table with no users");
		allUserPage.clickNewUserButton();
		Waiter.waitUntilUrlToBe(Constants.NEW_USER_PAGE_URL);

		// adding 5 users
		NewUserPage newUserPage = new NewUserPage();
		for (int i = 1; i <= 5; i++) {
			newUserPage.addUser("User" + i, "user" + i + "@testmail.com", "User" + i + "@123", "User" + i + "@123");
			Waiter.waitUntilUrlToBe(Constants.ALL_USER_PAGE_URL);
			if (i < 5) {
				driver.navigate().to(Constants.NEW_USER_PAGE_URL);
			}
		}

		// checking added users are available in table
		allUserPage = new AllUserPage();
		Assertions.assertTrue(allUserPage.isAnyUserRecords(), "Checking table with user records");

		List<Map<String, String>> tableRecords = allUserPage.getTableRecords();
		for (int i = 0; i < tableRecords.size(); i++) {
			Map<String, String> record = tableRecords.get(i);
			int temp = i + 1;
			Assertions.assertEquals(record.get("Name"), "User" + temp);
			Assertions.assertEquals(record.get("Email"), "user" + temp + "@testmail.com");
			Assertions.assertEquals(record.get("Password"), "User" + temp + "@123");
		}

	}

	@Test
	@Story("New user button")
	@Description("Checking whether clicking on New user button takes to new user page")
	public void testClickingNewUserButton() {
		allUserPage.clickNewUserButton();
		Waiter.waitUntilUrlToBe(Constants.NEW_USER_PAGE_URL);
		String pageHeader = new NewUserPage().getPageHeader();
		Assertions.assertTrue(StringUtils.equalsIgnoreCase("new user", pageHeader), "Checking New user page header");
	}
}
