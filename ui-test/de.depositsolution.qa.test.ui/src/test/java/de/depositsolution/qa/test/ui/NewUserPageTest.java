package de.depositsolution.qa.test.ui;

import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import de.depositsolution.qa.test.ui.commons.Constants;
import de.depositsolution.qa.test.ui.commons.Waiter;
import de.depositsolution.qa.test.ui.pages.AllUserPage;
import de.depositsolution.qa.test.ui.pages.NewUserPage;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;

/**
 * Test for New user page.
 * 
 * @author ramya
 *
 */
@Epic("New user page")
@Feature("New user creation feature")
public class NewUserPageTest extends BasePageTest {

	private NewUserPage newUserPage;

	@BeforeEach
	public void openPage() {
		driver.navigate().to(Constants.NEW_USER_PAGE_URL);
		newUserPage = new NewUserPage();
		String pageHeader = newUserPage.getPageHeader();
		Assertions.assertTrue(StringUtils.equalsIgnoreCase("new user", pageHeader), "Checking New user page header");
	}

	@Story("User creation with different possible inputs")
	@Description("Trying to create new user with different possible inputs name, email, password and confirmation password")
	@ParameterizedTest(name = "{7}")
	@CsvFileSource(resources = "/de/depositsolution/qa/test/ui/user-feed.csv", delimiter = '|')
	public void testCreatingNewUser(String name, String email, String pwd, String confirmPwd, String redirectUrl,
			String pageHeader, String errorMsgs, String testName) {

		newUserPage.addUser(name, email, pwd, confirmPwd);
		verifyPageRedirection(redirectUrl);
		if (StringUtils.isNotBlank(errorMsgs)) {
			assertValidationMessages(errorMsgs.split(","));
		}
	}

	@Test
	@Story("All user button")
	@Description("Checking whether clicking on All user button takes to all user page")
	public void testClickingAllUserButton() {
		newUserPage.clickAllUserButton();
		Waiter.waitUntilUrlToBe(Constants.ALL_USER_PAGE_URL);
		String pageHeader = new AllUserPage().getPageHeader();
		Assertions.assertTrue(StringUtils.equalsIgnoreCase("all user", pageHeader), "Checking All user page header");
	}

	/**
	 * Verified landed page.
	 * 
	 * @param redirectUrl
	 */
	private void verifyPageRedirection(String redirectUrl) {
		Waiter.waitUntilUrlToBe(redirectUrl);
		if (Constants.ALL_USER_PAGE_URL.equals(redirectUrl)) {
			AllUserPage allUserPage = new AllUserPage();
			Assertions.assertTrue(
					StringUtils.equalsIgnoreCase(Constants.PAGE_HEADER_ALL_USER_PAGE, allUserPage.getPageHeader()),
					"Checking All user page header");
		} else if (Constants.NEW_USER_PAGE_URL.equals(redirectUrl)) {
			NewUserPage newUserPage = new NewUserPage();
			Assertions.assertTrue(
					StringUtils.equalsIgnoreCase(Constants.PAGE_HEADER_NEW_USER_PAGE, newUserPage.getPageHeader()),
					"Checking new user page header");
		}
	}

	/**
	 * Method for asserting validation messages.
	 * 
	 * @param expectedErrorArr contains field and expected error
	 */
	private void assertValidationMessages(String[] expectedErrorArr) {
		newUserPage = new NewUserPage();
		for (String error : expectedErrorArr) {
			String[] split = error.split(":");
			String field = split[0].trim();
			String expectedError = split.length > 1 ? split[1].trim() : StringUtils.EMPTY;

			switch (field) {
			case Constants.FIELD_NAME:
				Assertions.assertEquals(expectedError, newUserPage.getUserNameErrorMessage(),
						"Checking user name validation error");
				break;
			case Constants.FIELD_EMAIL:
				Assertions.assertEquals(expectedError, newUserPage.getEmailErrorMessage(),
						"Checking email validation error");
				break;
			case Constants.FIELD_PASSWORD:
				Assertions.assertEquals(expectedError, newUserPage.getPasswordErrorMessage(),
						"Checking password validation error");
				break;
			case Constants.FIELD_CONFIRMATION_PASSWORD:
				Assertions.assertEquals(expectedError, newUserPage.getConfirmationPasswordErrorMessage(),
						"Checking confirmation password validation error");
				break;
			default:
				break;
			}
		}
	}
}
