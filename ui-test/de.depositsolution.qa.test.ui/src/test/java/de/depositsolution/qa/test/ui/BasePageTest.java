package de.depositsolution.qa.test.ui;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.WebDriver;

import de.depositsolution.qa.test.ui.commons.Commons;
import de.depositsolution.qa.test.ui.commons.Constants;

/**
 * Contains common test behaviors.
 * 
 * @author ramya
 *
 */
public class BasePageTest {

	protected static WebDriver driver;

	@BeforeAll
	public static void setup() {
		driver = Commons.getDriver(true);
		Assertions.assertTrue(Constants.RESPONSE_200 == Commons.deleteUsers(),
				"Checking delete user api reponse status");
	}

	@AfterAll
	public static void tearDown() {
		Commons.driverCleanup();
	}
}
